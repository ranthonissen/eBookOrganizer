﻿using System;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using Autofac;
using eBookOS.Interfaces.Services;
using eBookOS.Services.Builders;
using eBookOS.Services.Factories;
using eBookOS.Shared.Extensions;
using Seterlund.CodeGuard;
using eBookOS.Shared;

namespace eBookOS.ImportService
{
    public partial class ImportService : ServiceBase
    {
        private new static IContainer Container { get; set; }

        public ImportService()
        {
            BuildContainer();
            InitializeComponent();
        }

        private void BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<EBookFactory>().As<IEBookFactory>();
            builder.RegisterType<EBookImporterFactory>().As<IEBookImporterFactory>();
            Container = builder.Build();
        }

        protected override void OnStart(string[] args)
        {
            StartWatchingFolder(ConfigurationManager.AppSettings["baseFolder"]);
        }

        protected void StartWatchingFolder(string baseFolder)
        {
            var exists = Directory.Exists(baseFolder);
            Guard.That(baseFolder)
                .IsTrue(e => exists, "Folder does not exist: {0}".FormatWith(baseFolder));

            ProcessFilesInFolder(baseFolder);

            WatchFolder(baseFolder);
        }

        private void ProcessFilesInFolder(string baseFolder)
        {
            var directoryInfo = new DirectoryInfo(baseFolder);

            foreach (var file in directoryInfo.GetFiles())
            {
                ProcessFile(file.FullName);
            }
        }

        private void WatchFolder(string baseFolder)
        {
            var watcher = new FileSystemWatcher(baseFolder);

//            watcher.Changed += watcher_Changed;
            watcher.Created += watcher_Changed;
//            watcher.Renamed += watcher_Changed;

            watcher.EnableRaisingEvents = true;
        }

        void watcher_Changed(object sender, FileSystemEventArgs e)
        {
            ProcessFile(e.FullPath);
        }

        private void ProcessFile(string fullPath)
        {
            try
            {
                EBook eBook;

                using (var scope = Container.BeginLifetimeScope())
                {
                    var factory = scope.Resolve<IEBookFactory>();
                    eBook = factory.CreateEBook(fullPath);

                    Console.WriteLine("Filename: {0}".FormatWith(eBook.OriginalFilename));
                    Console.WriteLine();
                }

                using (var scope = Container.BeginLifetimeScope())
                {
                    var factory = scope.Resolve<IEBookImporterFactory>();
                    var importer = factory.CreateEBookImporter(eBook);

                    var eBookMaker = new EBookMaker(importer, eBook);
                    eBookMaker.ImportEBook();
                    eBook = eBookMaker.GetEBook();

                    Console.WriteLine(eBook.Title);
                    Console.WriteLine(eBook.Published);
                    Console.WriteLine(eBook.Publisher);
                    Console.WriteLine(eBook.Author);
                    Console.WriteLine(eBook.ISBN);
                    Console.WriteLine();
                }
            }
            catch (FileFormatNotSupportedException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
