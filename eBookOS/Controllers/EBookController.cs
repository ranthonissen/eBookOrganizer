﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using eBookOS.DataAccess;
using eBookOS.Shared;

namespace eBookOS.Controllers
{
    public class EBookController : Controller
    {
        private readonly EBooksContext _db = new EBooksContext();

        //
        // GET: /EBook/

        public ActionResult Index()
        {
            return View(_db.EBooks.ToList());
        }

        //
        // GET: /EBook/Details/5

        public ActionResult Details(Guid id)
        {
            EBook ebook = _db.EBooks.Find(id);
            if (ebook == null)
            {
                return HttpNotFound();
            }
            return View(ebook);
        }

        //
        // GET: /EBook/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /EBook/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EBook ebook)
        {
            if (ModelState.IsValid)
            {
                ebook.Id = Guid.NewGuid();
                _db.EBooks.Add(ebook);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ebook);
        }

        //
        // GET: /EBook/Edit/5

        public ActionResult Edit(Guid id)
        {
            EBook ebook = _db.EBooks.Find(id);
            if (ebook == null)
            {
                return HttpNotFound();
            }
            return View(ebook);
        }

        //
        // POST: /EBook/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EBook ebook)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(ebook).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ebook);
        }

        //
        // GET: /EBook/Delete/5

        public ActionResult Delete(Guid id)
        {
            EBook ebook = _db.EBooks.Find(id);
            if (ebook == null)
            {
                return HttpNotFound();
            }
            return View(ebook);
        }

        //
        // POST: /EBook/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            EBook ebook = _db.EBooks.Find(id);
            _db.EBooks.Remove(ebook);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}