﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using eBookOS.Interfaces.Services;
using eBookOS.Services.Factories;
using eBookOS.Shared;
using eBookOS.Shared.Extensions;

namespace eBookOS.Services
{
    public class FilenameParser : IFilenameParser
    {
        private const string PublishedPattern = @"(?<Month>\s\D{3})?\s(?<Year>\d{4})";
        private const string ISBNPattern = @"(?:\s)*(?<ISBN>(97(8|9))?\d{9}(\d|X))(?:\s)*";

        private const string CleanFilenamePattern = @"[_\.-]";

        private static readonly EBookFactory Factory;
        public EBook EBook { get; set; }

        private string _cleanFilename;

        private readonly string[] _knownPublishers =
        {
            "Apress",
            "ForDummies",
            "For Dummies",
            "FriendsofED",
            "AWP",
            "FD",
            "Leanpub",
            "Manning",
            "OReilly",
            "PragBook",
            "Packt",
            "Rocky",
            "Sybex",
            "Syngress",
            "Wiley",
            "Wrox"
        };
        private readonly string[] _garbage = { "RETAIL", "eBook", "repackb00k", "ePUB", "NEWSPAPER" };

        private readonly Dictionary<string, string> _knownTerms = new Dictionary<string,string>
        {
            {"ASP.NET", "asp net"}
        };

        static FilenameParser()
        {
            Factory = new EBookFactory();
        }

        public FilenameParser()
        {
            EBook = null;
            _cleanFilename = null;
        }

        public void Parse(string filename)
        {
            EBook = Factory.CreateEBook(filename);

            PreParseFilename();

            RemoveGarbage();
            ExtractISBN();
            ExtractPublisher();
            ExtractPublished();
            ExtractTitle();
        }

        private void PreParseFilename()
        {
            _cleanFilename = EBook.Filename.Substring(0, EBook.Filename.LastIndexOf(EBook.Extension));

            // replace known abbreviations
            // - publishers
            var knownAbbreviatedPublishers = new Dictionary<string, string>
            {
                {"FD.", "ForDummies."}, 
                {"PB.", "PragBook."}, 
                {"PP.", "Packt."}
            };
            foreach (var knownTerm in knownAbbreviatedPublishers)
            {
                _cleanFilename = _cleanFilename.ReplaceCaseInsensitive(knownTerm.Key, knownTerm.Value);
            }
        }

        private void ExtractISBN()
        {
            var re = new Regex(ISBNPattern);

            var match = re.Match(_cleanFilename);
            if (match.Success)
                EBook.ISBN = match.Value.Trim();

            _cleanFilename = _cleanFilename.RemoveCaseInsensitive(EBook.ISBN);
        }

        private void ExtractPublished()
        {
            var matches = Regex.Match(_cleanFilename, PublishedPattern, RegexOptions.RightToLeft).Groups;
            var list = matches.Cast<Group>()
                .Select(grp => grp.Value.Trim())
                .Skip(1)
                .Where(value => !string.IsNullOrEmpty(value))
                .ToList();

            if (list.Any())
                EBook.Published = string.Join(" ", list);

            _cleanFilename = _cleanFilename.RemoveCaseInsensitive(EBook.Published);
        }

        private void ExtractTitle()
        {
            EBook.Title = _cleanFilename;
        }

        private void ExtractPublisher()
        {
            foreach (var knownPublisher in _knownPublishers
                .Where(knownPublisher => _cleanFilename.ToLower().Contains(knownPublisher.ToLower())))
            {
                EBook.Publisher = knownPublisher;
                break;
            }

            _cleanFilename = _cleanFilename.RemoveCaseInsensitive(EBook.Publisher);
        }

        private void RemoveGarbage()
        {
            // spaces
            _cleanFilename = Regex.Replace(_cleanFilename, CleanFilenamePattern, " ");

            // restore dots and hyphens for known terms
            foreach (var knownTerm in _knownTerms)
            {
                _cleanFilename = _cleanFilename.ReplaceCaseInsensitive(knownTerm.Value, knownTerm.Key);
            }

            // remove releasers garbage
            foreach (var value in _garbage)
            {
                _cleanFilename = _cleanFilename.RemoveCaseInsensitive(value);
            }
        }
    }
}