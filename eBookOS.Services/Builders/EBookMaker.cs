﻿using eBookOS.Interfaces.Services;
using eBookOS.Shared;

namespace eBookOS.Services.Builders
{
    public class EBookMaker
    {
        private readonly IEBookImporter _importer;
        private EBook _eBook;

        public EBookMaker(IEBookImporter importer, EBook eBook)
        {
            _importer = importer;
            _eBook = eBook;
        }

        public void ImportEBook()
        {
            _importer.ExtractInformation();

            _eBook = _importer.GetEBook();
        }

        public EBook GetEBook()
        {
            return _eBook;
        }
    }
}