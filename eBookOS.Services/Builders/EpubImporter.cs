﻿using eBookOS.Interfaces.Services;
using eBookOS.Shared;

namespace eBookOS.Services.Builders
{
    public class EpubImporter : EBookImporter
    {
        public EpubImporter(EBook eBook, IFilenameParser filenameParser) : base(eBook, filenameParser)
        {
        }

        public override void ExtractInformation()
        {
            throw new System.NotImplementedException();
        }
    }
}