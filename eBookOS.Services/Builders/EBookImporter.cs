using eBookOS.Interfaces.Services;
using eBookOS.Shared;

namespace eBookOS.Services.Builders
{
    public abstract class EBookImporter : IEBookImporter
    {
        protected readonly EBook EBook;

        public IFilenameParser FilenameParser { get; set; }

        protected EBookImporter(EBook eBook, IFilenameParser filenameParser)
        {
            EBook = eBook;
            FilenameParser = filenameParser;
        }

        public EBook GetEBook()
        {
            return EBook;
        }

//        public abstract void ExtractPublished();
//        public abstract void ExtractISBN();
//        public abstract void ExtractPublisher();
//        public abstract void ExtractTitle();
//        public abstract void ExtractAuthor();
//        public abstract void ParseName();

        public abstract void ExtractInformation();
    }
}