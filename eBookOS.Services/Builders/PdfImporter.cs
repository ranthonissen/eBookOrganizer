﻿using System.IO;
using eBookOS.Interfaces.Services;
using eBookOS.Shared;
using iTextSharp.text.pdf;

namespace eBookOS.Services.Builders
{
    public class PdfImporter : EBookImporter
    {
        private readonly PdfReader _reader;

        // Subjext
        // Keywords

        public PdfImporter(EBook eBook, IFilenameParser filenameParser) : base(eBook, filenameParser)
        {
            if (!File.Exists(eBook.OriginalFilename))
            {
                throw new FileNotFoundException(eBook.OriginalFilename);
            }

            filenameParser.Parse(EBook.OriginalFilename);
         
            _reader = new PdfReader(eBook.OriginalFilename);
        }

        private void ExtractNumberOfPages()
        {
            EBook.NumberOfPages = _reader.NumberOfPages;
        }

        public void ExtractTitle()
        {
            EBook.Title = _reader.Info.ContainsKey("Title")
                ? _reader.Info["Title"]
                : "";
        }

        public void ExtractAuthor()
        {
            EBook.Author = _reader.Info.ContainsKey("Author")
                ? _reader.Info["Author"]
                : "";
        }

        public override void ExtractInformation()
        {
            ExtractAuthor();
            ExtractTitle();
            ExtractNumberOfPages();
            ExtractKeywords();
        }

        private void ExtractKeywords()
        {
            EBook.Keywords = _reader.Info.ContainsKey("Keywords")
                ? _reader.Info["Keywords"].Split(';')
                : new string[0];

            for (var i = 0; i < EBook.Keywords.Length; i++)
            {
                EBook.Keywords[i] = EBook.Keywords[i].Trim();
            }
        }
    }
}