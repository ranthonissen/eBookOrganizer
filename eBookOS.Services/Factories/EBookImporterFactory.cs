﻿using eBookOS.Domain;
using eBookOS.Interfaces.Services;
using eBookOS.Services.Builders;
using eBookOS.Shared;

namespace eBookOS.Services.Factories
{
    public class EBookImporterFactory : IEBookImporterFactory
    {
        public IEBookImporter CreateEBookImporter(EBook eBook)
        {
            IFilenameParser filenameParser = new FilenameParser();

            switch (eBook.Extension.ToLower())
            {
                case PdfEBook.FileExtension:
                    return new PdfImporter(eBook, filenameParser);
                case ChmEBook.FileExtension:
                    return new ChmImporter(eBook, filenameParser);
                case EpubEBook.FileExtension:
                    return new EpubImporter(eBook, filenameParser);
            }

            throw new FileFormatNotSupportedException(eBook.Extension.ToLower());

        }
    }
}