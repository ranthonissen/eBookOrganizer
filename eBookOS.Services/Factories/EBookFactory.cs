﻿using System.IO;
using eBookOS.Domain;
using eBookOS.Interfaces.Services;
using eBookOS.Shared;

namespace eBookOS.Services.Factories
{
    public class EBookFactory : IEBookFactory
    {
        public EBook CreateEBook(string fullPath)
        {
            //if (File.Exists(fullPath))
            //{
                var fileInfo = new FileInfo(fullPath);

                switch (fileInfo.Extension)
                {
                    case PdfEBook.FileExtension:
                        return new PdfEBook(fullPath);
                    case ChmEBook.FileExtension:
                        return new ChmEBook(fullPath);
                    case EpubEBook.FileExtension:
                        return new EpubEBook(fullPath);
                    case MobiEbook.FileExtension:
                        return new MobiEbook(fullPath);
                }

                throw new FileFormatNotSupportedException(fileInfo.Extension);
            //}
            //
            //throw new FileNotFoundException(fullPath);
        }
    }
}