using System;
using eBookOS.Shared.Extensions;

namespace eBookOS.Shared
{
    public class FileFormatNotSupportedException : ApplicationException
    {
        public FileFormatNotSupportedException(string extension)
            : base("This file format is not supported: {0}".FormatWith(extension))
        {
        }
    }
}