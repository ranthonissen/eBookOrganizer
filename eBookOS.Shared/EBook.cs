﻿using System;
using System.IO;

namespace eBookOS.Shared
{
    public abstract class EBook
    {
        public static string FileExtension = "";

        protected EBook(string fullPath)
        {
            OriginalFilename = fullPath;

            var file = new FileInfo(OriginalFilename);

            Filename = file.Name;
            Extension = file.Extension;
        }

        public Guid Id { get; set; }

        public string OriginalFilename { get; protected set; }
        public string Filename { get; protected set; }
        public string Extension { get; protected set; }

        public string Title { get; set; }
        public string ISBN { get; set; }
        public string Published { get; set; }
        public string Publisher { get; set; }
        public string Author { get; set; }
        public int NumberOfPages { get; set; }
        public string[] Keywords { get; set; }
    }
}