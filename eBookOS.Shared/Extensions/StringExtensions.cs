using System.Text.RegularExpressions;

namespace eBookOS.Shared.Extensions
{
    public static class StringExtensions
    {
         public static string FormatWith(this string format, params string[] stringArgs)
         {
             return string.Format(format, stringArgs);
         }

         public static string ReplaceCaseInsensitive(this string value, string oldValue, string newValeu)
        {
            if (value.ToLower().Contains(oldValue.ToLower()))
            {
                var before = value.Substring(0, value.ToLower().IndexOf(oldValue.ToLower()));
                var after = value.Substring(value.ToLower().IndexOf(oldValue.ToLower()) + oldValue.Length);
                return "{0}{1}{2}".FormatWith(before, newValeu, after);
            }
             return value;
        }

         public static string RemoveCaseInsensitive(this string value, string oldValue)
         {
             if (oldValue == null) return value;

             string result;
             if (value.ToLower().StartsWith(oldValue.ToLower()))
             {
                 result = value.Substring(oldValue.Length).TrimStart();
             }
             else if (value.ToLower().EndsWith(oldValue.ToLower()))
             {
                 result = value.Substring(0, value.IndexOf(oldValue)).TrimEnd();
             }
             else
             {
                 result = value.ReplaceCaseInsensitive(oldValue, " ");
             }
             return Regex.Replace(result, @"[\s]+", " ");
        }
    }
}