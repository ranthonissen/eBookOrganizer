﻿using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using eBookOS.Shared;
using eBookOS.Shared.Extensions;
using eBookOS.Tests.Shared;
using Ploeh.AutoFixture;
using Xunit;

namespace eBookOS.Services.Tests
{
    public class FilenameParserTests : AAATest<FilenameParser>
    {
        private string _filename;
        private string _fullPathname;

        public override void Arrange()
        {
            var folder = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var filename = Fixture.Create("filename");
            var extension = "pdf";

            _filename = "{0}.{1}".FormatWith(filename, extension.Substring(0,3));
            _fullPathname = @"{0}\{1}".FormatWith(folder, _filename);
        }

        public override FilenameParser CreateSUT()
        {
            return new FilenameParser();
        }

        public override void Act()
        {
            SUT.Parse(_fullPathname);
        }

        [Fact]
        public void ShouldAssignTheOriginalFilename()
        {
            Assert.Equal(_fullPathname, SUT.EBook.OriginalFilename);
            Assert.Equal(_filename, SUT.EBook.Filename);
        }
    }

    public class ListOfFilesTestData : AAATest<FilenameParser>
    {
        private string[] _testData;
        private IDictionary<string, EBook> _results;

        public override FilenameParser CreateSUT()
        {
            return new FilenameParser();
        }

        public override void Arrange()
        {
            _results = new Dictionary<string, EBook>();
            _testData = File.ReadAllLines(ConfigurationManager.AppSettings["folderTestData"] + "\\" + "FilenameParser.txt");
        }

        public override void Act()
        {
                foreach (var testFile in _testData)
                {
                    var file = testFile.Split('\t')[0];
                    SUT.Parse(file);
                    _results.Add(testFile, SUT.EBook);
                }
        }

        [Fact]
        public void ShouldBeAbleToParseAllTheFileNames()
        {
            foreach (var result in _results)
            {
                Assert.NotEmpty(result.Value.Title);
                var testData = result.Key.Split('\t');
                var eBook = result.Value;

                Assert.NotEmpty(eBook.OriginalFilename);
                Assert.NotEmpty(eBook.Filename);
                Assert.NotEmpty(eBook.Extension);
                Assert.NotEmpty(eBook.Title);

                Debug.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}".FormatWith(eBook.OriginalFilename, eBook.Title, eBook.Published, eBook.Publisher, eBook.ISBN));

                Assert.Equal(testData[1], eBook.Title ?? "");
                Assert.Equal(testData[2], eBook.Published ?? "");
                Assert.Equal(testData[3], eBook.Publisher ?? "");
                Assert.Equal(testData[4], eBook.ISBN ?? "");
            }
        }
    }
    public abstract class SpecificFilenameParserTest : AAATest<FilenameParser>
    {
        protected string _filename;
        private string _fullPathname;

        public override FilenameParser CreateSUT()
        {
            return new FilenameParser();
        }

        public override void Arrange()
        {
            var folder = System.Reflection.Assembly.GetExecutingAssembly().Location;

            _fullPathname = @"{0}\{1}".FormatWith(folder, _filename);
        }

        public override void Act()
        {
            SUT.Parse(_fullPathname);
        }
    }

    public class SpecificFilenameParserTest_0131857258_Pdf : SpecificFilenameParserTest
    {
        public override void Arrange()
        {
            _filename = "0131857258.pdf";
            base.Arrange();
        }

        [Fact]
        public void ShouldExtractTheISBN()
        {
            Assert.Equal(_filename, SUT.EBook.Filename);
            Assert.Equal("", SUT.EBook.Title);
            Assert.Null(SUT.EBook.Publisher);
            Assert.Null(SUT.EBook.Author);
            Assert.Null(SUT.EBook.Published);
            Assert.Equal("0131857258", SUT.EBook.ISBN);
        }
    }

    public class SpecificFilenameParserTest_1430247738_Apress_ASP_NET_MVC_4_Recipes_2013_RETAIL_eBook_repackb00k_pdf : SpecificFilenameParserTest
    {
        public override void Arrange()
        {
            _filename = "1430247738_Apress.ASP.NET.MVC.4.Recipes.2013.RETAIL.eBook-repackb00k.pdf";
            base.Arrange();
        }

        [Fact]
        public void ShouldExtractTheRightInformation()
        {
            Assert.Equal(_filename, SUT.EBook.Filename);
            Assert.Equal("2013", SUT.EBook.Published);
            Assert.Equal("Apress", SUT.EBook.Publisher);
            Assert.Equal("ASP.NET MVC 4 Recipes", SUT.EBook.Title);
            Assert.Equal("1430247738", SUT.EBook.ISBN);
            Assert.Null(SUT.EBook.Author);
        }
    }

    public class SpecificFilenameParserTest_1430249323_Apress___Beginning_jQuery_2013_RETAiL_ePUB_eBOOk_NEWSPAPER_epub : SpecificFilenameParserTest
    {
        public override void Arrange()
        {
            _filename = "1430249323_Apress.-.Beginning.jQuery.2013.RETAiL.ePUB.eBOOk-NEWSPAPER.epub";
            base.Arrange();
        }

        [Fact]
        public void ShouldExtractTheRightInformation()
        {
            Assert.Equal(_filename, SUT.EBook.Filename);
            Assert.Equal("2013", SUT.EBook.Published);
            Assert.Equal("Apress", SUT.EBook.Publisher);
            Assert.Equal("Beginning jQuery", SUT.EBook.Title);
            Assert.Equal("1430249323", SUT.EBook.ISBN);
            Assert.Null(SUT.EBook.Author);
        }
    }

    public class SpecificFilenameParserTest_Agile_Principles__Patterns__and_Practices_in_C__pdf : SpecificFilenameParserTest
    {
        public override void Arrange()
        {
            _filename = "Agile Principles, Patterns, and Practices in C#.pdf";
            base.Arrange();
        }

        [Fact]
        public void ShouldExtractTheRightInformation()
        {
            Assert.Equal(_filename, SUT.EBook.Filename);
            Assert.Null(SUT.EBook.Published);
            Assert.Null(SUT.EBook.Publisher);
            Assert.Equal("Agile Principles, Patterns, and Practices in C#", SUT.EBook.Title);
            Assert.Null(SUT.EBook.ISBN);
            Assert.Null(SUT.EBook.Author);
        }
    }

    public class SpecificFilenameParserTest_Apress_ASP_NET_MVC_4_and_the_Web_API_Feb_2013_epub : SpecificFilenameParserTest
    {
        public override void Arrange()
        {
            _filename = "Apress.ASP.NET.MVC.4.and.the.Web.API.Feb.2013.epub";
            base.Arrange();
        }

        [Fact]
        public void ShouldExtractTheRightInformation()
        {
            Assert.Equal(_filename, SUT.EBook.Filename);
            Assert.Equal("Feb 2013", SUT.EBook.Published);
            Assert.Equal("Apress", SUT.EBook.Publisher);
            Assert.Equal("ASP.NET MVC 4 and the Web API", SUT.EBook.Title);
            Assert.Null(SUT.EBook.ISBN);
            Assert.Null(SUT.EBook.Author);
        }
    }

    public class SpecificFilenameParserTest_For_Dummies_HTML5_Canvas_For_Dummies_Dec_2012_epub : SpecificFilenameParserTest
    {
        public override void Arrange()
        {
            _filename = "For.Dummies.HTML5.Canvas.For.Dummies.Dec.2012.epub";
            base.Arrange();
        }

        [Fact]
        public void ShouldExtractTheRightInformation()
        {
            Assert.Equal(_filename, SUT.EBook.Filename);
            Assert.Equal("Dec 2012", SUT.EBook.Published);
            Assert.Equal("For Dummies", SUT.EBook.Publisher);
            Assert.Equal("HTML5 Canvas For Dummies", SUT.EBook.Title);
            Assert.Null(SUT.EBook.ISBN);
            Assert.Null(SUT.EBook.Author);
        }
    }

    public class SpecificFilenameParserTest_FriendsofED_Foundation_Adobe_Edge_Animate_Dec_2012_pdf : SpecificFilenameParserTest
    {
        public override void Arrange()
        {
            _filename = "FriendsofED.Foundation.Adobe.Edge.Animate.Dec.2012.pdf";
            base.Arrange();
        }

        [Fact]
        public void ShouldExtractTheRightInformation()
        {
            Assert.Equal(_filename, SUT.EBook.Filename);
            Assert.Equal("Dec 2012", SUT.EBook.Published);
            Assert.Equal("FriendsofED", SUT.EBook.Publisher);
            Assert.Equal("Foundation Adobe Edge Animate", SUT.EBook.Title);
            Assert.Null(SUT.EBook.ISBN);
            Assert.Null(SUT.EBook.Author);
        }
    }

    /*
    + 1430247738_Apress.ASP.NET.MVC.4.Recipes.2013.RETAIL.eBook-repackb00k.pdf
    + 1430249323_Apress.-.Beginning.jQuery.2013.RETAiL.ePUB.eBOOk-NEWSPAPER.epub
    + Agile Principles, Patterns, and Practices in C#.pdf
    + Apress.ASP.NET.MVC.4.and.the.Web.API.Feb.2013.epub
    Automating Microsoft Windows Server 2008 R2 with Windows PowerShell 2.0.pdf
    + For.Dummies.HTML5.Canvas.For.Dummies.Dec.2012.epub
    + FriendsofED.Foundation.Adobe.Edge.Animate.Dec.2012.pdf
    Manning.Effective.Unit.Testing.Feb.2013.pdf
    Microsoft.Press.Windows.PowerShell.3.0.Step.by.Step.Feb.2013.pdf
    MK.Web.Services.Service-Oriented.Architectures.and.Cloud.Computing.2nd.Edition.Jan.2013.pdf
    NHibernate 3 Beginner's Guide.pdf
    NHibernate_3.0_Cookbook___70_Incredibly_Powerful_Recipes_for_Using_the_Full_Spectrum_of_Solutions_in_the_NHibernate_Ecosystem.pdf
    No.Starch.Blender.Master.Class.Feb.2013.pdf
    Oreilly.EPUB.3.Best.Practices.Jan.2013.pdf
    Packt.Twitter.Bootstrap.Web.Development.How-To.2012.RETAIL.eBook-ELOHiM.pdf
    Packtpub.Git.Version.Control.for.Everyone.Jan.2013.pdf
    Pragmatic.Core.Data.2nd.Edition.Jan.2013.pdf
    Pragmatic.Scalable.and.Modular.Architecture.for.CSS.2012.RETAIL.eBook-ELOHiM.pdf
    progit.en.pdf
    progit.epub
    r-ban45.pdf
    SitePoint.Jump.Start.CoffeeScript.2012.RETAIL.eBook-ELOHiM.pdf
    Wiley.Teach.Yourself.VISUALLY.PowerPoint.2013.Mar.2013.pdf
    Wrox.Beginning.Visual.Csharp.2012.Programming.Dec.2012.epub
    */
}