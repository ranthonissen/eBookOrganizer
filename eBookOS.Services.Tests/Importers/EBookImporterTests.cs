﻿using Autofac;
using eBookOS.Interfaces.Services;
using eBookOS.Services.Builders;
using eBookOS.Shared;
using eBookOS.Tests.Shared;
using eBookOS.Tests.Shared.Fixtures;
using FakeItEasy;
using Xunit;

namespace eBookOS.Services.Tests.Importers
{
    public class EBookImporterTests : AAATest<EBookImporter>
    {
        private EBook _eBook;
        private IFilenameParser _filenamerParser;
        private EBook _importedEbook;

        public override void Arrange()
        {
            _eBook = A.Fake<EBook>(e => e.WithArgumentsForConstructor(new object[] { EBookFixture.TestFilename }));
            _filenamerParser = A.Fake<IFilenameParser>();
            Builder.RegisterInstance(_filenamerParser).As<IFilenameParser>();
        }

        public override EBookImporter CreateSUT()
        {
            return new PdfImporter(_eBook, _filenamerParser);
        }

        public override void Act()
        {
            _importedEbook = SUT.GetEBook();
        }

        [Fact]
        public void ImportEBookShouldSetTheEBook()
        {
            Assert.NotNull(_importedEbook);
            Assert.Same(_eBook, _importedEbook);
        }
    }
}