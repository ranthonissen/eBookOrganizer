﻿using eBookOS.Interfaces.Services;
using eBookOS.Services.Builders;
using eBookOS.Shared;
using eBookOS.Tests.Shared;
using FakeItEasy;
using Xunit;

namespace eBookOS.Services.Tests.Importers
{
    public class EBookMakerTests : AAATest<EBookMaker>
    {
        private EBook _eBook;
        private IEBookImporter _importer;

        public override void Arrange()
        {
            _eBook = A.Fake<EBook>(e => e.WithArgumentsForConstructor(new object[] { "123.abc" }));
            _importer = A.Fake<IEBookImporter>();
             A.CallTo(() => _importer.GetEBook()).Returns(_eBook);
       }

        public override EBookMaker CreateSUT()
        {
            return new EBookMaker(_importer, _eBook);
        }

        public override void Act()
        {
            SUT.ImportEBook();
        }

        [Fact]
        public void ShouldCallExtractInformationFromTheImporter()
        {
            A.CallTo(() => _importer.ExtractInformation()).MustHaveHappened();
        }

        [Fact]
        public void GetEBookShouldReturnTheEbookFromTheImporter()
        {
            var importedEbook = SUT.GetEBook();

            Assert.NotNull(importedEbook);
            Assert.Same(_eBook, importedEbook);
        }
    }
}