﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using eBookOS.Domain;
using eBookOS.Interfaces.Services;
using eBookOS.Services.Builders;
using eBookOS.Shared;
using eBookOS.Shared.Extensions;
using eBookOS.Tests.Shared;
using FakeItEasy;
using Xunit;

namespace eBookOS.Services.Tests.Importers
{
    public class PdfImporterTests : AAATest<PdfImporter>
    {
        private PdfEBook _eBook;
        private IFilenameParser _filenameParser;
        private EBook _importedBook;
        private string _fullPath;

        public override void Arrange()
        {
            _fullPath = @"{0}\TestFiles\{1}".FormatWith(
                Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName,
                "Prentice.Hall.The.Clean.Coder.May.2011.pdf");
            _eBook = new PdfEBook(_fullPath);
            _eBook = A.Fake<PdfEBook>(e => e.WithArgumentsForConstructor(() => new PdfEBook(_fullPath)));
            _filenameParser = A.Fake<IFilenameParser>();

//            builder.RegisterInstance(_filenameParser).As<IFilenameParser>();
        }

        public override PdfImporter CreateSUT()
        {
            return new PdfImporter(_eBook, _filenameParser);
        }

        public override void Act()
        {
            SUT.ExtractInformation();
            _importedBook = SUT.GetEBook();
        }

        [Fact]
        public void ImportEBook_ShouldSetEBookInstance()
        {

            Assert.NotNull(_importedBook);
            Assert.Same(_eBook, _importedBook);
        }

        //ExtractArchive();

        //ParseName();

        [Fact]
        public void ParseName_ShouldCallTheFileNameParser()
        {
            A.CallTo(() => _filenameParser.Parse(_fullPath)).MustHaveHappened();
        }

        [Fact]
        public void ShouldExtractTheTitle()
        {
            Assert.Equal("The Clean Coder: A Code of Conduct For Professional Programmers", _importedBook.Title);
        }

        [Fact]
        public void ShouldExtractTheAuthor()
        {
            Assert.Equal("Robert C. Martin", _importedBook.Author);
        }

        [Fact]
        public void ShouldExtractTheNumberOfPages()
        {
            Assert.Equal(244, _importedBook.NumberOfPages);
        }

        //ExtractAuthor();

        //ExtractTitle();

        //ExtractPublisher();

        //ExtractEvenMoreData();
    }

    public class PdfImporterTestFromActualFile : AAATest<IEnumerable<PdfImporter>>
    {
        private string[] _testData;
        private IList<EBook> _results;
        private IFilenameParser _filenameParser;
        private string _folderTestData;

        public override void Arrange()
        {
            _folderTestData = ConfigurationManager.AppSettings["folderTestData"] + "\\";
            _testData = File.ReadAllLines(_folderTestData + "PdfImporterTestFromActualFiles.txt");
            _filenameParser = A.Fake<IFilenameParser>();
            _results = new List<EBook>();
        }

        public override IEnumerable<PdfImporter> CreateSUT()
        {
            return _testData.Select(testFile => 
                testFile.Split('\t')[0]).Select(file =>
                    new PdfEBook(string.Format("{0}PdfImporterTestFromActualFiles\\{1}", _folderTestData, file))).Select(eBook => 
                        new PdfImporter(eBook, _filenameParser));
        }

        public override void Act()
        {
            foreach (var pdfImporter in SUT)
            {
                pdfImporter.ExtractInformation();
                var importedBook = pdfImporter.GetEBook();
                _results.Add(importedBook);
            }
        }

        [Fact]
        public void ShouldHaveProcessedTheFiles()
        {
            Assert.NotEmpty(_results);
        }

        [Fact]
        public void ShouldMaintainTheInformationFromTheFilenameParser()
        {
            foreach (var result in _results)
            {
                Assert.NotNull(result.OriginalFilename);
                Assert.NotNull(result.Filename);
                Assert.NotNull(result.Extension);
            }
        }

        [Fact]
        public void ShouldExtractTheAvailableInformationFromTheFiles()
        {
            foreach (var result in _results)
            {
                Debug.WriteLine("{0}\t| {1}\t| {2}\t| {3}\t| {4}".FormatWith(
                    result.Filename, result.NumberOfPages.ToString(), result.Author, result.Title, String.Join(",", result.Keywords)));

                Assert.NotNull(result.NumberOfPages);
                Assert.NotNull(result.Author);
                Assert.NotNull(result.Title);
                Assert.NotNull(result.Keywords);
            }
        }

        [Fact]
        public void ShouldNotSetInformationNotAvailableInTheTheFiles()
        {
            foreach (var result in _results)
            {
                Assert.Null(result.ISBN);
                Assert.Null(result.Published);
                Assert.Null(result.Publisher);
            }
        }
    }
}