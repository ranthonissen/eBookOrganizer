﻿using eBookOS.Shared;

namespace eBookOS.Interfaces.Services
{
    public interface IEBookFactory
    {
        EBook CreateEBook(string fullPath);
    }
}