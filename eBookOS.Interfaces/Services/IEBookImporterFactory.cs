﻿using eBookOS.Shared;

namespace eBookOS.Interfaces.Services
{
    public interface IEBookImporterFactory
    {
        IEBookImporter CreateEBookImporter(EBook eBook);
    }
}