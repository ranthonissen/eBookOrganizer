﻿using eBookOS.Shared;

namespace eBookOS.Interfaces.Services
{
    public interface IEBookImporter
    {
        IFilenameParser FilenameParser { get; set; }

        EBook GetEBook();
//        void ExtractPublisher();
//        void ExtractTitle();
//        void ExtractAuthor();
//        void ParseName();
//        void ExtractPublished();
//        void ExtractISBN();
        void ExtractInformation();
    }
}