﻿using eBookOS.Shared;

namespace eBookOS.Interfaces.Services
{
    public interface IFilenameParser
    {
        void Parse(string filename);
        EBook EBook { get; }
    }
}