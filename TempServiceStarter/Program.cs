﻿using System;
using System.Configuration;
using eBookOS.ImportService;

namespace TempServiceStarter
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            (new ServiceStarter()).StartService(ConfigurationManager.AppSettings["baseFolder"]);

            // Wait for the user to quit the program.
            Console.WriteLine("Press \'q\' to quit.");
            while (Console.Read() != 'q') { }
        }

        private class ServiceStarter : ImportService
        {

            public void StartService(string baseFolder)
            {
                StartWatchingFolder(baseFolder);
            }
        }
    }
}