﻿using eBookOS.Shared;

namespace eBookOS.Domain
{
    public class EpubEBook : EBook
    {
        public new const string FileExtension = ".epub";

        public EpubEBook(string fullPath)
            : base(fullPath)
        {
            Extension = FileExtension;
        }
    }
}