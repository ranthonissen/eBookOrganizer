﻿using eBookOS.Shared;

namespace eBookOS.Domain
{
    public class PdfEBook : EBook
    {
        public new const string FileExtension = ".pdf";

        public PdfEBook(string fullPath)
            : base(fullPath)
        {
            Extension = FileExtension;
        }
    }
}