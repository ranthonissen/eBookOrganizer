﻿using eBookOS.Shared;

namespace eBookOS.Domain
{
    public class ChmEBook : EBook
    {
        public new const string FileExtension = ".chm";

        public ChmEBook(string fullPath)
            : base(fullPath)
        {
            Extension = FileExtension;
        }
    }
}