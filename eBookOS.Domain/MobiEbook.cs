﻿using eBookOS.Shared;

namespace eBookOS.Domain
{
    public class MobiEbook : EBook
    {
        public new const string FileExtension = ".mobi";

        public MobiEbook(string fullPath)
            : base(fullPath)
        {
            Extension = FileExtension;
        }
    }
}