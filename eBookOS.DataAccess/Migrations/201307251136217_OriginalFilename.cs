namespace eBookOS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OriginalFilename : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EBooks", "OriginalFilename", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EBooks", "OriginalFilename");
        }
    }
}
