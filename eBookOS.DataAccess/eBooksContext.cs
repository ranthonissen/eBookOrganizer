﻿using System.Data.Entity;
using eBookOS.Domain;
using eBookOS.Shared;

namespace eBookOS.DataAccess
{
    public class EBooksContext : DbContext
    {
        public DbSet<EBook> EBooks { get; set; }
    }
}
