﻿using System;

namespace eBookOS.Tests.Shared.Fixtures
{
    static class NumberFixture
    {
        static readonly Random Random = new Random((int) DateTime.Now.Ticks);

        public static int Integer(int maxValue)
        {
            return Random.Next(maxValue);
        }
    }
}
