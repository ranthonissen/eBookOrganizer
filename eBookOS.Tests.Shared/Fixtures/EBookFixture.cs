﻿using System;
using System.IO;
using System.Linq;

namespace eBookOS.Tests.Shared.Fixtures
{
    public static class EBookFixture
    {
        public static string TestFilename {
            get
            {
                var directory = new DirectoryInfo(String.Format(@"{0}\TestFiles",
                    Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName));
                var files = directory.GetFiles();

                return files[NumberFixture.Integer(files.Count())].FullName;
            }
        }
    }
}
