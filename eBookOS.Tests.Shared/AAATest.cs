﻿using Autofac;
using Ploeh.AutoFixture;
using IContainer = Autofac.IContainer;

namespace eBookOS.Tests.Shared
{
    public abstract class AAATest<T>
    {
        protected static IContainer Container { get; set; }

        protected Fixture Fixture = new Fixture();
        protected ContainerBuilder Builder;

        protected AAATest()
        {
            Builder = new ContainerBuilder();
            Arrange();
//            RegisterDependencies();
            Container = Builder.Build();
            SUT = CreateSUT();
            Act();
        }

        public T SUT;
        public abstract void Arrange();
        public abstract T CreateSUT();
        public abstract void Act();
    }
}
